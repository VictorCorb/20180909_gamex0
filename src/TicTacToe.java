import java.util.Scanner;

public class TicTacToe {

    public static void showGame(String[][] game) {
//        is doua [] ca sa mentioneze ca e o matrice
        for (int i = 0; i < game.length; i++) {
            for (int j = 0; j < game.length; j++) {
                if (game[i][j] == null) {
                    System.out.print(" \t");
                } else {
                    System.out.print(game[i][j] + "\t");
                }
            }
            System.out.println();
        }

    }

    public static boolean testLine(String[][] game, int line, String sign) {

//        forced test of all elements in a line
//        if (sign.equals(game[line][0]) && sign.equals(game[line][1]) && sign.equals(game[line][2])) {
//            return true;
//        }else{
//            return false;
//        }
//        alta metoda mai jos
        boolean win = true;
        int i = 0;
        while (i < game.length && win == true) {
            if (!sign.equals(game[line][i])) {
                win = false;
            }
            i++;
        }
        return win;
    }

    public static boolean testGameIsWin(String[][] game, int line, int column, String sign) {
        boolean win = true;
        win = testLine(game, line, sign);
        if (win == false) {
            win = testColumn(game, column, sign);
        }
        if (win == false) {
//            apelez metoda care testeaza daca diagonala principala este castigatoare
        }
        if (win == false) {
//            apelez metoda care testeaza daca diag secundara este castigatoare

        }
        return win;
    }

    //    se folosesc metode diferite pt linii, coloane, diag
    public static boolean testColumn(String[][] game, int column, String sign) {
        int i = 0;
        boolean win = true;
        if (!sign.equals(game[i][column])) {
            win = false;
        }
        while (i < game.length - 1 && win == true) {
            if (game[i][column] != null && game[i + 1][column] != null) {
                if (!game[i][column].equals(game[i + 1][column])) {
                    win = false;
                }
                i++;

            } else {
                win = false;

            }
        }
        return win;
    }


    public static void main(String[] args) {
        int n = 3;
//        starea jocului
        String[][] game = new String[n][n];
//        numele jucatorilor (0 si 1)
        String[] players = new String[2];
//        semnele din joc "X" si "0" atribuite jucatorilor 0 si 1
        String[] signs = new String[]{"X", "0"};
//        jucatorul curent
        Integer currentPlayer = 0;

//        numarul de mutari efectuate pana acum
        Integer numberOfMoves = 0;

        Scanner scanner = new Scanner(System.in);
//        introdu numele jucatorilor
        System.out.println("Jucatorul 0(X): ");
        players[0] = scanner.nextLine();
        System.out.println("Jucatorul 1(0): ");
        players[1] = scanner.nextLine();

//        afisam starea jocului
        showGame(game);
        System.out.println("Incepe jocul:");

        boolean win = false;
//        jucam jocul
        while (numberOfMoves < 9 && win == false) {
//            citim mutarea exemplu 2,1 -> linia 2, coloana 1
            System.out.println("jucatorul " + players[currentPlayer] + " muta");
            String move = scanner.nextLine();
//            separam mutarea in coordonate
            String[] coords = move.split(",");
//            transformam din String in Integer
            Integer line = Integer.valueOf(coords[0]);
            Integer column = Integer.valueOf(coords[1]);
//            mutam
            game[line][column] = signs[currentPlayer];
            numberOfMoves++;
            if (numberOfMoves >= (n + n - 1)) {
                win = testGameIsWin(game, line, column, signs[currentPlayer]);
            }
            showGame(game);

            if (win == false) {

//            change player
//            if (currentPlayer == 0) {
//                currentPlayer = 1;
//            }else{
//                currentPlayer = 0;
//            }
//            sau varianta de mai jos pt change player
                currentPlayer = (currentPlayer == 0) ? 1 : 0;
            }
        }
        if (win) {
            System.out.println("Felicitari " + players[currentPlayer] + ", ai castigat");
        }else{
            System.out.println("Nici un jucator nu a castigat");
        }
    }
}
